item.series.data.reduce((acc, val) => {
    return acc + (val[0] + ";" + val[1] + "\n");
}, "");


((input) => {
    let header = ";";
    let data = "";

    for(let i = 0; i < input.length; i++) {
        const region = input[i];

        header += region.label + ";";
    }

    for(let j = 0; j < input[0].data.length; j++) {
        const time = input[0].data[j][0];
        
        let row = time + ";";

        for(let i = 0; i < input.length; i++) {
            const region = input[i];
            const bandwidth = region.data[j][1];

            row += bandwidth + ";";
        }

        data += row + "\n";
    }

    return header + "\n" + data;


})( JSON.parse( data[ 'json' ] ));