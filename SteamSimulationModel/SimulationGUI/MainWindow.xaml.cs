﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SteamSimulationModel;
using System.Threading;

namespace SimulationGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private AsyncNetwork network;

        private List<int[]> users = new List<int[]>();
        private List<Polyline> lines = new List<Polyline>();

        private int min = int.MaxValue, max = int.MinValue;
        private Polyline axis;

        SolidColorBrush[] brushes = new SolidColorBrush[] {
                new SolidColorBrush(Colors.Red),        // NA
                new SolidColorBrush(Colors.White),      // Europe
                new SolidColorBrush(Colors.Yellow),     // Asia
                new SolidColorBrush(Colors.Blue),       // Russia
                new SolidColorBrush(Colors.Wheat),      // SA
                new SolidColorBrush(Colors.LightGray),  // CA
                new SolidColorBrush(Colors.Black),      // Africa
                new SolidColorBrush(Colors.Brown),      // ME
                new SolidColorBrush(Colors.Turquoise),  // Oceania
            };

        public MainWindow()
        {
            network = new AsyncNetwork(InitialConditions.Create());
            network.Simulate(InitialConditions.SIMULATION_TIME, InitialConditions.TIMESTEP);

            min = Math.Min(network.Region.Min(region => region.MinValue), min);
            max = Math.Max(network.Region.Max(region => region.MaxValue), max);

            InitializeComponent();

            DrawAxis();

            for(var i = 0; i < network.Region.Length; i++)
            {
                var polyline = new Polyline();
                polyline.Stroke = brushes[i];
                lines.Add(polyline);
                polyline.FillRule = FillRule.EvenOdd;

                canvas.Children.Add(polyline);
            }


            CompositionTarget.Rendering += CompositionTarget_Rendering;

        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            var width = canvas.Width = this.Width - 25;
            var height = canvas.Height = this.Height - 50;

            
            if(network.IsSimulating)
            {
                var regions = network.Region.Length;

                var time = network.Time;
                var result = network.Iterate();

                var x = Utils.getMappedRangeValue(0, (float)width, 0, InitialConditions.SIMULATION_TIME, time);

                for (var i = 0; i < regions; i++)
                {
                    var saturation = result.Saturation[i];
                    var userCount = result.Users[i];
                    var y_users = Utils.getMappedRangeValue(0, (float)height, max, min, userCount);
                    var y_saturation = Utils.getMappedRangeValue(0, (float)height, 1, 0, saturation);


                    lines[i].Points.Add(new Point(x, y_users));

                    /*var circle = new EllipseGeometry(new Point(x, y_saturation), 1, 1);
                    var path = new Path();
                    path.Stroke = brushes[i];
                    path.Data = circle;
                    canvas.Children.Add(path);*/
                }
            }

        }

        private void DrawAxis()
        {

            axis = new Polyline();
            axis.Stroke = Brushes.Black;
            axis.StrokeThickness = 2;
            axis.FillRule = FillRule.EvenOdd;

            canvas.Children.Add(axis);
        }

        private void Network_OnIterationComplete(Network self, float time, RegionIterationResult result)
        {
            int regions = self.Region.Count();

            var users = new int[regions];

            min = Math.Min(self.Region.Min(region => region.MinValue), min);
            max = Math.Max(self.Region.Max(region => region.MaxValue), max);



            for (int i = 0; i < regions; i++)
            {
                var userCount = users[i] = result.Users[i];

                var x = Utils.getMappedRangeValue(0, (float)this.Width, 0, InitialConditions.SIMULATION_TIME, time);
                var y = Utils.getMappedRangeValue(0, (float)this.Height, min, max, userCount);

                var circle = new EllipseGeometry(new Point(x, y), 1, 1);
                var path = new Path();
                path.Stroke = new SolidColorBrush(Colors.Red);
                path.Data = circle;
                canvas.Children.Add(path);
            }

            canvas.InvalidateVisual();
            //this.UpdateLayout();
            // Thread.Sleep(500);
        }


        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            var width = this.Width - 25;
            var height = this.Height - 50;

            axis.Points = new PointCollection
            {
                new Point(0, 0),
                new Point(0, height),
                new Point(width, height)
            };
        }
    }
}
