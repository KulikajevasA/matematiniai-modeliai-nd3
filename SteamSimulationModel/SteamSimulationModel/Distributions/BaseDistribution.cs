﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel.Distributions
{
    public abstract class BaseDistribution
    {
        //public BaseDistribution(int )
        
        public abstract double[] GenerateSamples(int nSamples);
    }
}
