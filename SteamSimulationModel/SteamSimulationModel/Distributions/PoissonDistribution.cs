﻿using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel.Distributions
{
    public class PoissonDistribution : BaseDistribution
    {
        private Poisson distribution;

        public PoissonDistribution(float lambda)
        {
            distribution = new Poisson(lambda);
            //var samples = new int[50];
            //distribution.Samples(samples);
        }

        public override double[] GenerateSamples(int nSamples)
        {
            var intSamples = new int[nSamples];

            distribution.Samples(intSamples);

            var @out = new double[nSamples];

            for (var i = 0; i < nSamples; i++) @out[i] = intSamples[i];

            return @out;
        }
    }
}
