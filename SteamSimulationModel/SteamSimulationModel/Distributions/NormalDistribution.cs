﻿using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel.Distributions
{
    public class NormalDistribution : BaseDistribution
    {
        private Normal distribution;
        private double multiplier;

        public NormalDistribution(double multiplier, float mean = 0, float stdev = 1)
        {
            this.multiplier = multiplier;
            distribution = new Normal(mean, stdev);
        }

        public override double[] GenerateSamples(int nSamples)
        {
            var samples = new double[nSamples];

            distribution.Samples(samples);

            //for (var i = 0; i < nSamples; i++) samples[i] *= multiplier;

            return samples;
        }
    }
}
