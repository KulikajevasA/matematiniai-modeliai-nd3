﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class AsyncNetwork
    {
        private Network network;

        public bool IsSimulating { get; private set; }
        public bool SimulationComplete { get; private set; }

        public Region[] Region { get { return network.Region; } }

        private float[] currentQueue = new float[0];
        private float[] currentOverflow = new float[0];
        private float iterator, simulationTime, timestep;

        public float Time { get { return iterator; } }

        public AsyncNetwork(Network network)
        {
            this.network = network;
        }

        public void Simulate(float simulationTime, float timestep)
        {
            SimulationComplete = false;
            IsSimulating = true;
            iterator = 0;
            this.timestep = timestep;
            this.simulationTime = simulationTime;

            currentQueue = new float[network.Region.Length];
            currentOverflow = new float[network.Region.Length];

            network.RebuildSamples((int)(Math.Round(1 / timestep)));
        }

        public RegionIterationResult Iterate()
        {
            if (!IsSimulating) throw new Exception("Start simulation first");

            var result = network.Calculate(iterator, currentQueue, currentOverflow, false);

            iterator += timestep;

            if (simulationTime < iterator)
            {
                IsSimulating = false;
                SimulationComplete = true;
            }

            return result;
        }
    }
}
