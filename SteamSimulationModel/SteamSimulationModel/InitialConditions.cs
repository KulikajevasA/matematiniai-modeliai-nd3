﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class InitialConditions
    {
        public static readonly int[] SALE_DAYS = new int[] { /*0, 4, 8*/ };
        public const int SIMULATION_TIME = 10 * Utils.DAY;
        public const float TIMESTEP = 1f / 60f;

        private InitialConditions() { }

        public static Network Create()
        {
            return new Network(new Region[] {
                    new Region("North America", 0.1575f, 3775870, 1200f, -8),
                    new Region("Europe", 0.1137f, 4321073, 1200f, 0),
                    new Region("Asia", 0.1181f, 4255739, 1200f, +8),
                    new Region("Russia", 0.1763f, 1047601, 300f, +3),
                    new Region("South America", 0.2507f, 781758, 200f, -4),
                    new Region("Central America", 0.4f, 22529, 7f, -6),
                    new Region("Africa", 0.5f, 36046, 12f, +2),
                    new Region("Middle East", 0.2754f, 310901, 100f, +3),
                    new Region("Oceania", 0.237f, 304142, 90f, +9)
                });

            /*return new Network(new Region[] {
                    new Region("North America", 0.1575f, 3775870, 0, -8),
                    new Region("Europe", 0.1137f, 0, 1200f, 0),
                    new Region("Asia", 0.1181f, 4255739, 0, +8),
                    new Region("Asia", 0.1181f, 0, 1200f, +8),
                });*/
        }
    }
}
