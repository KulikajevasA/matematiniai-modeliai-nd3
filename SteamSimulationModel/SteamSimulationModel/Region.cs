﻿using MathNet.Numerics.Distributions;
using SteamSimulationModel.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class Region
    {
        public const float GIGABITS_TO_KILOBITS = 1024f * 1024;
        public const float RESONSE_TIME = 1f;

        public string Name { get; private set; }
        public int ExpectedPeakUserCount { get; private set; }
        public int LeastExpectedUsers { get; private set; }
        public float Bandwidth { get; private set; }
        public int TimeZone { get; private set; }

        private static Random random = new Random();
        private int elementsInHour;

        private int[] samples = new int[0];
        private bool[] reachable = new bool[0];

        private BaseDistribution userDistribution;

        public int GetUsers(float timepoint)
        {
            int index = (int)Math.Floor(timepoint % 24 * elementsInHour);

            return this.samples[index];
        }

        public bool GetAvailability(float timepoint)
        {
            int index = (int)Math.Floor(timepoint % 24 * elementsInHour);

            return this.reachable[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Name of the region.</param>
        /// <param name="leastUsersRatio">Least expected users ratio.</param>
        /// <param name="peakUsers">Exected peak users.</param>
        /// <param name="bandwidth">Used bandwidth in Gbps.</param>
        /// <param name="timezone">Offset from UTC+0 timezone.</param>
        public Region(string name, float leastUsersRatio, int peakUsers, float bandwidth, int timezone)
        {
            peakUsers = (int)Math.Round(peakUsers * 1.42f);

            this.TimeZone = timezone;
            this.LeastExpectedUsers = (int)Math.Round((leastUsersRatio) * peakUsers);
            this.ExpectedPeakUserCount = peakUsers;
            this.Name = name;
            this.Bandwidth = bandwidth * GIGABITS_TO_KILOBITS;

            //this.userDistribution = new PoissonDistribution(peakUsers);
            this.userDistribution = new NormalDistribution(peakUsers);
            //var intSamples = distribution.GenerateSamples(10000);


            //System.Console.WriteLine("{0} has {1} users.", name, distribution.GetAtTimePoint(12));

        }


        public float GetUsage(float timepoint)
        {
            int userCount = GetUsers(timepoint);
            //int userCount = (int)Math.Round((float)random.NextDouble() * ExpectedPeakUserCount);
            //float bandwidthMultiplier = (float)Math.Max(1f, random.NextDouble() * 1.25f);
            float bandwidthMultiplier = 1;

            float usage = userCount * User.USED_BANDWIDTH * bandwidthMultiplier;

            //float saturation = usage / Bandwidth;

            return usage;
        }

        public int MaxValue { get { return samples.Max(); } }
        public int MinValue { get { return samples.Min(); } }

        public int[] Samples() { return this.samples; }

        private int[] GenerateUsers(float peak, int sampleCount)
        {

            var samples = new int[sampleCount];

            if (peak > 0)
            {
                var distribution = new Poisson(peak);
                distribution.Samples(samples);
            }
            return samples;
        }

        public void CreateSamples(int elementsInHour)
        {
            this.elementsInHour = elementsInHour;
            var sampleCount = elementsInHour * Utils.DAY;
            var samples = new int[sampleCount];
            var hours = (int)Math.Ceiling(sampleCount / (float)elementsInHour);
            var timeOffset = User.PEAK_HOUR - 12 + TimeZone;

            var lambdas = new int[Utils.DAY];
            var toPeak = (int)Math.Floor(Utils.DAY / 2f);

            for (var i = 0; i < toPeak; i++)
            {
                int index = (Utils.DAY + i + timeOffset) % Utils.DAY;
                int indexPrev = (Utils.DAY * 2 - i + timeOffset - 1) % Utils.DAY;

                lambdas[index] = lambdas[indexPrev] = (int)Utils.getMappedRangeValue(LeastExpectedUsers, ExpectedPeakUserCount, 0, toPeak - 1, i);
            }

            for (var i = 0; i < hours; i++)
            {
                var genSamples = GenerateUsers(lambdas[i], elementsInHour);

                Array.Copy(genSamples, 0, samples, i * elementsInHour, elementsInHour);
            }

            this.samples = samples;

            this.reachable = new bool[sampleCount];

            var normal = new Normal();

            for (var i = 0; i < sampleCount; i++)
                this.reachable[i] = Math.Abs(normal.Sample()) <= 3;
        }

        [Obsolete]
        public void CreateSamplesOld(int sampleCount, int elementsInHour)
        {
            this.elementsInHour = elementsInHour;

            var ordereredSamples = this.userDistribution
                .GenerateSamples(sampleCount)
                .AsQueryable()
                .OrderBy(i => i)
                .ToArray();

            if (samples.Length != sampleCount) samples = new int[sampleCount];

            var timeOffset = User.PEAK_HOUR - 12 + TimeZone;

            for (int i = 0, j = 0; i < sampleCount; i++)
            {
                if (i % 2 == 0)
                {
                    int index = (j + sampleCount + timeOffset * elementsInHour) % sampleCount;
                    this.samples[index] = (int)Math.Round(Math.Abs(ordereredSamples[i]));
                }
                else
                {
                    int index = (sampleCount - 1 - j + sampleCount + timeOffset * elementsInHour) % sampleCount;
                    this.samples[index] = (int)Math.Round(ordereredSamples[i]);
                    j++;
                }
            }
        }
    }
}
