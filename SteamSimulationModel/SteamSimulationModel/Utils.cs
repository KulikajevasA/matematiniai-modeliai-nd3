﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class Utils
    {
        public const int DAY = 24;
        public const int SALE_TIME = 17;
        public const string OUTPUT_DIR = @"C:\Users\audry\Desktop\matematika_nd3\output";

        public static float getMappedRangeValue(float a, float b, float c, float d, float x, bool clamp = false)
        {
            float r = (b - a) / (d - c);
            float ans = (x - c) * r + a;

            return clamp ? Math.Max(Math.Min(ans, b), a) : ans;
        }
    }
}
