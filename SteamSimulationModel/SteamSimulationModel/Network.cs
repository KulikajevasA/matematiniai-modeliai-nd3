﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class Network
    {
        public const float CROSS_REGION_LATENCY = 1.25f;
        public const float PROCESSING_TIME_LATENCY = 1.1f;

        public Region[] Region { get; private set; }

        //public delegate void IterationComplete(Network self, float time, RegionIterationResult result);
        //public event IterationComplete OnIterationComplete;

        public Network(Region[] region)
        {
            this.Region = region;
        }

        public RegionIterationResult Calculate(float timepoint, float[] overflow, float[] queues, bool isSaleDay)
        {
            var saturation = new float[Region.Length];
            var users = new int[Region.Length];
            //var queues = new float[region.Length];

            for (var i = 0; i < Region.Length; i++)
            {
                var curRegion = Region[i];
                users[i] = curRegion.GetUsers(timepoint);

                bool isReachable = curRegion.GetAvailability(timepoint);

                var multiplier = isSaleDay && (timepoint % 24 >= Utils.SALE_TIME && timepoint % 24 <= 24)
                    ? Utils.getMappedRangeValue(3, 1, Utils.SALE_TIME, 24, timepoint % 24)
                    : 1;
                var newBandwidth = curRegion.GetUsage(timepoint) * multiplier;

                var usedBandwidth = newBandwidth + overflow[i];

                if (isReachable && curRegion.Bandwidth > 0)
                {
                    var curSaturation = Math.Min(usedBandwidth / curRegion.Bandwidth, 1.0f);

                    saturation[i] = Math.Min(curSaturation, 1);
                    overflow[i] = Math.Max(0, curSaturation - 10) * curRegion.Bandwidth;

                    queues[i] = Math.Max(queues[i] - 1f, 0) + curSaturation * PROCESSING_TIME_LATENCY;
                    //queues[i] = Math.Max(queues[i] + curSaturation * PROCESSING_TIME_LATENCY - 2f, 0) ;
                }
                else
                {
                    saturation[i] = 1;
                    overflow[i] = usedBandwidth;
                    queues[i] = 0;
                }
            }

            for (var i = 0; i < Region.Length; i++)
            {
                var requiredBandwidth = overflow[i];
                var transferableBandwidth = requiredBandwidth;

                if (requiredBandwidth <= 0) continue;

                float addionalQueue = 0;

                for (var j = 0; j < Region.Length; j++)
                {
                    if (requiredBandwidth <= 0) break;
                    if (j == i || Region[j].Bandwidth <= 0) continue;

                    var additionalSaturation = requiredBandwidth / Region[j].Bandwidth;
                    var curSaturation = saturation[j];

                    var newSaturation = additionalSaturation + curSaturation;

                    var dataOverflow = Math.Max(0f, newSaturation - 1f) * Region[j].Bandwidth;
                    requiredBandwidth = dataOverflow;
                    saturation[j] = Math.Min(1f, newSaturation);

                    var transferedBandwidth = transferableBandwidth - requiredBandwidth;
                    addionalQueue += transferableBandwidth / Region[j].Bandwidth;
                }


                overflow[i] = requiredBandwidth;
                queues[i] += addionalQueue * PROCESSING_TIME_LATENCY * CROSS_REGION_LATENCY;
                //queues[i] += (transferableBandwidth - requiredBandwidth) / Region[i].Bandwidth * PROCESSING_TIME_LATENCY * CROSS_REGION_LATENCY;
            }

            return new RegionIterationResult()
            {
                Saturation = saturation,
                Users = users,
                Overflow = overflow,
                Queues = queues
            };
        }

        public static void ClearToEndOfCurrentLine()
        {
            int currentLeft = Console.CursorLeft;
            int currentTop = Console.CursorTop;
            Console.Write(new String(' ', Console.WindowWidth - currentLeft));
            Console.SetCursorPosition(currentLeft, currentTop);
        }

        public void RebuildSamples(int elementsInHour)
        {
            foreach (var region in Region) region.CreateSamples(elementsInHour);
        }

        [Obsolete]
        private void RebuildSamples(int samples, int elementsInHour)
        {
            foreach (var region in Region) region.CreateSamples(elementsInHour);
        }

        public void SaveUserSamples(StreamWriter streamWriter)
        {

            //int samples = (int)Math.Round(1 / InitialConditions.TIMESTEP);
            int samples = 25;

            RebuildSamples(samples);

            for (int i = 0, len = samples * Utils.DAY; i < len; i++)
            {
                var userCount = Region.Aggregate(0, (accumulator, region) =>
                {
                    return accumulator + region.Samples()[i];
                });

                streamWriter.WriteLine(userCount);
            }
        }

        public void Simulate(float simulationTime, float timestep, int[] saleDays, bool save = false, int iterations = 1)
        {
            for (int k = 0; k < iterations; k++)
            {
                using (StreamWriter fs = save ? new StreamWriter(string.Format(@"{0}\queue_{1}.csv", Utils.OUTPUT_DIR, k)) : null)
                {
                    Console.CursorVisible = false;

                    var queues = new float[Region.Length];
                    var overflow = new float[Region.Length];

                    var capacity = Region.Sum(x => x.Bandwidth);

                    //int samplesPerDay = (int)Math.Round(Utils.DAY / timestep);

                    RebuildSamples(/*samplesPerDay,*/ (int)(Math.Round(1 / timestep)));
                    int lastSamplesBuilt = 0;

                    for (float time = 0, i = 0; time <= simulationTime; time += timestep, i++)
                    {
                        int day = (int)Math.Floor(time / Utils.DAY);

                        bool isSaleDay = saleDays.Contains(day);

                        if (lastSamplesBuilt != day)
                        {
                            lastSamplesBuilt = day;
                            RebuildSamples(/*samplesPerDay,*/ (int)(Math.Round(1 / timestep)));
                        }

                        var result = Calculate(time, overflow, queues, isSaleDay);

                        var totalOverflow = overflow.Sum();
                        //var saturationAverage = result.Saturation.Average();
                        //var queuesAverage = queues.Average();

                        float saturationAverage = 0;
                        for (var j = 0; j < Region.Length; j++)
                            saturationAverage += result.Saturation[j] * (Region[j].Bandwidth / capacity);

                        float queuesAverage = 0;
                        for (var j = 0; j < Region.Length; j++)
                            queuesAverage += result.Queues[j] * (Region[j].Bandwidth / capacity);


                        //System.Console.Clear();
                        //System.Console.SetCursorPosition(0, 0);

                        Console.SetCursorPosition(0, 0);

                        Console.WriteLine(new String('-', Console.WindowWidth - 1));
                        ClearToEndOfCurrentLine();
                        Console.WriteLine("{0, 25}: {1, 20} {2, 5} Time: {3, 16:0.##} h ({4, 4:0.##})", "Iteration", i, "|", time, time % 24);
                        Console.WriteLine(new String('-', Console.WindowWidth - 1));

                        for (var j = 0; j < Region.Length; j++)
                        {
                            ClearToEndOfCurrentLine();

                            if (result.Saturation[j] >= 1) Console.ForegroundColor = ConsoleColor.Red;
                            else if (result.Saturation[j] >= 0.75) Console.ForegroundColor = ConsoleColor.Yellow;

                            Console.WriteLine("{0, 25}: {1, 20:0.##} Gbs | Saturation: {2, 10:0.##} % | Queue: {3, 10:0.##} | Users: {4, 10:0.##} %",
                                Region[j].Name,
                                overflow[j] / SteamSimulationModel.Region.GIGABITS_TO_KILOBITS,
                                result.Saturation[j] * 100f,
                                queues[j],
                                result.Users[j] / (float)Region[j].ExpectedPeakUserCount * 100f
                                );

                            Console.ResetColor();
                        }

                        Console.WriteLine(new String('-', Console.WindowWidth - 1));
                        ClearToEndOfCurrentLine();
                        Console.WriteLine("{0, 10}{1, 15}: {2, 20:0.##} Gbs | Saturation: {3, 10:0.##} % | Queue: {4, 10:0.##}",
                            "Totals",
                            "Overload",
                            totalOverflow / SteamSimulationModel.Region.GIGABITS_TO_KILOBITS,
                            saturationAverage * 100f,
                            queuesAverage
                            );
                        Console.WriteLine(new String('-', Console.WindowWidth - 1));

                        //fs.WriteLine(string.Format("{0};{1};{2}", time / 24f, saturationAverage, queuesAverage));

                        if (save) fs.WriteLine(string.Format("{0}", queuesAverage));

                        //Thread.Sleep(10);
                    }
                }
            }
        }
    }
}
