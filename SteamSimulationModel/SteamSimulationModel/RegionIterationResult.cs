﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    public class RegionIterationResult
    {
        public float[] Saturation { get; set; }
        public int[] Users { get; set; }

        public float[] Queues { get; set; }
        public float[] Overflow { get; set; }
    }
}
