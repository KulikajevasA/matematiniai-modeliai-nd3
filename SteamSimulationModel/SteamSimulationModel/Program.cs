﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamSimulationModel
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!Directory.Exists(Utils.OUTPUT_DIR)) Directory.CreateDirectory(Utils.OUTPUT_DIR);

            Console.SetWindowSize(150, Console.WindowHeight);

            var network = InitialConditions.Create();

            //SaveSamples(network);

            network.Simulate(InitialConditions.SIMULATION_TIME, InitialConditions.TIMESTEP, InitialConditions.SALE_DAYS, false, 1);

            System.Console.ReadLine();
        }

        private static void SaveSamples(Network network)
        {
            using (var fs = new StreamWriter(string.Format(@"{0}\users.txt", Utils.OUTPUT_DIR)))
            {
                network.SaveUserSamples(fs);
            }
        }
    }
}
