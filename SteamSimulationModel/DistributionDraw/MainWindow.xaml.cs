﻿using SteamSimulationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DistributionDraw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            canvas.Width = this.Width - 25;
            canvas.Height = this.Height - 50;

            canvas.Children.Clear();

            DrawAxis();
            DrawDistribution();
        }


        private void DrawAxis()
        {

            var height = canvas.ActualHeight;
            var width = canvas.ActualWidth;


            var poly = new Polyline();
            poly.Stroke = Brushes.Black;
            poly.StrokeThickness = 2;
            poly.FillRule = FillRule.EvenOdd;
            poly.Points = new PointCollection
            {
                new Point(0, 0),
                new Point(0, height),
                new Point(width, height)
            };

            canvas.Children.Add(poly);
        }

        private void DrawDistribution()
        {
            var height = canvas.ActualHeight;
            var width = canvas.ActualWidth;

            //var region = new Region("North America", 10, 1200f, -8);
            //var region = new Region("North America", 0.1575f, 3775870, 1200f, -8);
            var region =
                new Region("Europe", 0.1137f, 4321073, 1200f, 0)
                ;
            float timestep = 1f / 60f;
            int samplesPerDay = (int)Math.Round(24 / timestep);
            region.CreateSamples((int)(Math.Round(1 / timestep)));

            var poly = new Polyline();
            poly.Stroke = Brushes.Red;
            poly.StrokeThickness = 2;
            poly.FillRule = FillRule.EvenOdd;
            poly.Points = new PointCollection();

            int min = region.MinValue, max = region.MaxValue;

            var pixelsX = width / samplesPerDay;
            var pixelsY = height / region.MaxValue;

            var samples = region.Samples();

            for (var i = 0; i < samples.Length; i++)
                poly.Points.Add(new Point(pixelsX * i, Utils.getMappedRangeValue(0, (float)height, min, max, samples[i])));

            canvas.Children.Add(poly);
        }
    }
}
